datatype Terminal = Term of string;

datatype Nonterminal = Nonterm of string;

datatype symbols = T of Terminal | NT of Nonterminal;

type rhs = symbols list;

datatype lhs = Lhs of Nonterminal;

type rule = lhs * rhs;

type grammar = rule list;

(*
val myGrammar : grammar = [ ( Lhs(Nonterm("A")), [T(Term("a")), NT(Nonterm("B"))]), 
			    ( Lhs(Nonterm("B")), []) ];
*)

structure StringKey = 
struct
	type ord_key = string;
	val compare = String.compare;
end;

structure Map = RedBlackMapFn(StringKey);
val nullDic = Map.empty;

(*################################### COMPUTING NULLABLE ###################################*)

(*Paramaters : rhs and dictionary*)
fun defineNullable 	[] nullDic = true
|   defineNullable      (T(Term(x)) :: xs) nullDic = false
|   defineNullable      (NT(Nonterm(x)) :: xs) nullDic = if ( (Map.find(nullDic, x)) = NONE) then
							   false
							else
							   defineNullable xs nullDic;
(*Parameters : grammar and dictionary*)
fun insertNullable [] nullDic = nullDic
|   insertNullable ((Lhs(Nonterm(x)), rhs) :: rest) nullDic = 
					if (defineNullable rhs nullDic = true) then
						insertNullable rest (Map.insert(nullDic, x, true))
					else 
						insertNullable rest nullDic;


(*Parameters : grammar, size of the dictionary, dictionary*)

fun iteration (myGrammar, mapSize, nullDic) =  
		let
			val dic = insertNullable myGrammar nullDic
			val newSize = (Map.numItems(dic))
		in
			if (newSize = mapSize) then
				dic
			else
				iteration (myGrammar, newSize, dic)
		end;
val myGrammar : grammar = [ ( Lhs(Nonterm("A")), [T(Term("a")), NT(Nonterm("B"))]), 
			    ( Lhs(Nonterm("B")), []) ];

val nullItems = iteration (myGrammar, 0, Map.empty);

Map.listItemsi(nullItems);

(*################################### COMPUTING FIRST ###################################*)


fun keyval NONE = []
|   keyval x   = valOf(x);

fun isolate [] = []
|   isolate (x :: xs) = x :: isolate (List.filter(fn y => y <> x) xs);

(*Parameters : nonterminal rhs firstMap nullItems*)
fun rhsFirst (ruleLhs : string) [] firstMap nullItems = firstMap
|   rhsFirst (ruleLhs : string) (T(Term(x)) :: xs) firstMap nullItems = 
		let
			val firstList = keyval(Map.find(firstMap, ruleLhs))
		in
			Map.insert (firstMap, ruleLhs, isolate([x] @ firstList))
		end
|   rhsFirst (ruleLhs : string) (NT(Nonterm(x)) :: xs) firstMap nullItems = 
		let
			val firstList = keyval(Map.find(firstMap, ruleLhs))
		in
			if (Map.find(nullItems, x) = NONE) then
				Map.insert(firstMap,ruleLhs,isolate(keyval(Map.find(firstMap,x)) @ firstList))
			else
				rhsFirst ruleLhs xs (Map.insert(firstMap,ruleLhs,isolate(firstList))) nullItems 
		end;

fun calcFirst [] firstMap _ = firstMap
|   calcFirst ((Lhs(Nonterm(x)), rhs) :: rest) firstMap nullItems = 
				
				calcFirst rest (rhsFirst x rhs firstMap nullItems) nullItems;

fun iterateFirst myGrammar firstMap nullItems mapSize =  
		let
			val dic = calcFirst myGrammar firstMap nullItems
			val newSize = (Map.numItems(firstMap))
		in
			if (newSize = mapSize) then
				dic
			else
				iterateFirst myGrammar dic nullItems newSize
		end;

val firstMap = Map.empty;

val firstMap = iterateFirst myGrammar firstMap nullItems 0;

Map.listItemsi(nullItems);

Map.listItemsi(firstMap);










