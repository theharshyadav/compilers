signature ORD =
sig

type t

val le : t-> t-> bool

end

structure IntOrd : ORD =
struct

type t = int

fun le x y = x <= y;

end
