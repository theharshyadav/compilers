signature C_TOKENS =
sig
type linenum = int
type token
val VAR: linenum * linenum -> token
val WHILE: linenum * linenum -> token
val ELSE: linenum * linenum -> token
val IF: linenum * linenum -> token
val ASSIGN: linenum * linenum -> token
val OR: linenum * linenum -> token
val AND: linenum * linenum -> token
val GE: linenum * linenum -> token
val GT: linenum * linenum -> token
val LE: linenum * linenum -> token
val LT: linenum * linenum -> token
val NEQ: linenum * linenum -> token
val EQ: linenum * linenum -> token
val DIVIDE: linenum * linenum -> token
val MULT: linenum * linenum -> token
val MINUS: linenum * linenum -> token
val PLUS: linenum * linenum -> token
val RBRACE: linenum * linenum -> token
val LBRACE: linenum * linenum -> token
val RPAREN: linenum * linenum -> token
val LPAREN: linenum * linenum -> token
val SEMICOLON: linenum * linenum -> token
val COMMA: linenum * linenum -> token
val INT: (int) * linenum * linenum -> token
val ID: (string) * linenum * linenum -> token
val RETURN: linenum * linenum -> token
val EOF: linenum * linenum -> token
end
