type pos = int
type lexresult = Tokens.token

val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos
fun err(p1,p2) = ErrorMsg.error p1

fun str2int(s) = foldl (fn(a,r) => ord(a) - ord(#"0") + 10*r) 0 (explode s)
fun eof() =
let
    val
        pos = hd(!linePos)
    in
        Tokens.EOF(pos,pos)
    end

%%


%%
[\ \t\b\f\r]+ => (continue());
\n => (lineNum := !lineNum + 1; linePos := yypos::(!linePos); continue());
"+" => (Tokens.PLUS(yypos, yypos + 1));
"-" => (Tokens.MINUS(yypos, yypos + 1));
"*" => (Tokens.MULT(yypos, yypos + 1));
"/" => (Tokens.DIVIDE(yypos, yypos + 1));
"," => (Tokens.COMMA(yypos, yypos + 1));
"&" => (Tokens.AND(yypos, yypos + 1));
"|" => (Tokens.OR(yypos, yypos + 1));
"==" => (Tokens.EQ(yypos, yypos + 2));
"=" => (Tokens.ASSIGN(yypos, yypos + 1));
">=" => (Tokens.GE(yypos, yypos + 2));
">" => (Tokens.GT(yypos, yypos + 1));
"<=" => (Tokens.LE(yypos, yypos + 2));
"<" => (Tokens.LT(yypos, yypos + 1));
"!=" => (Tokens.NEQ(yypos, yypos + 2));
";" => (Tokens.SEMICOLON(yypos, yypos + 1));
"{" => (Tokens.LBRACE(yypos, yypos + 1));
"}" => (Tokens.RBRACE(yypos, yypos + 1));
"(" => (Tokens.LPAREN(yypos, yypos + 1));
")" => (Tokens.RPAREN(yypos, yypos + 1));
"var" => (Tokens.VAR(yypos, yypos + 3));
"while" => (Tokens.WHILE(yypos, yypos + 5));
"else" => (Tokens.ELSE(yypos, yypos + 4));
"if" => (Tokens.IF(yypos, yypos + 2));
"return" => (Tokens.RETURN(yypos, yypos + 6));
\"([a-zA-Z0-9_\ ]+)\" => (Tokens.ID(yytext, yypos, yypos + size yytext));
[a-zA-Z_]+ => (Tokens.ID(yytext, yypos, yypos + size yytext));
[0-9]+ => (Tokens.INT(str2int yytext, yypos, yypos + size yytext));

. => (ErrorMsg.error yypos ("illegal character " ^ yytext); continue());
